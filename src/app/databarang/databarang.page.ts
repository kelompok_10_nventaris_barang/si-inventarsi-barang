import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DashboardPage } from '../dashboard/dashboard.page';
import { DetailbarangPage } from '../detailbarang/detailbarang.page';
import { TambahbarangPage } from '../tambahbarang/tambahbarang.page';

@Component({
  selector: 'app-databarang',
  templateUrl: './databarang.page.html',
  styleUrls: ['./databarang.page.scss'],
})
export class DatabarangPage implements OnInit {

  constructor(public modalCtrl: ModalController) { }
  


  ngOnInit() {
  }
  async dashboard() {
    const modal = await this.modalCtrl.create({
      component: DashboardPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  async tambahbarang() {
    const modal = await this.modalCtrl.create({
      component: TambahbarangPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async detailbarang() {
    const modal = await this.modalCtrl.create({
      component: DetailbarangPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
