import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LaporanPage } from '../laporan/laporan.page';

@Component({
  selector: 'app-detaillaporan',
  templateUrl: './detaillaporan.page.html',
  styleUrls: ['./detaillaporan.page.scss'],
})
export class DetaillaporanPage implements OnInit {

  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }
  async laporan() {
    const modal = await this.modalController.create({
      component:LaporanPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
