import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TransaksiPage } from '../transaksi/transaksi.page';

@Component({
  selector: 'app-tambahtransaksi',
  templateUrl: './tambahtransaksi.page.html',
  styleUrls: ['./tambahtransaksi.page.scss'],
})
export class TambahtransaksiPage implements OnInit {

  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }
  async transaksi() {
    const modal = await this.modalController.create({
      component: TransaksiPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
