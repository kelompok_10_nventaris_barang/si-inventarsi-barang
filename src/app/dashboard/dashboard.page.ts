import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { DatabarangPage } from '../databarang/databarang.page';
import { LaporanPage } from '../laporan/laporan.page';
import { LoginPage } from '../login/login.page';
import { TransaksiPage } from '../transaksi/transaksi.page';
import { AngularFireAuth } from 'angularfire2/auth';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  constructor(private menu: MenuController,
    public modalController: ModalController,
    public auth: AngularFireAuth
    ) { }
  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  openEnd() {
    this.menu.open('end');
  }

  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }

  ngOnInit() {
  }
  selectedSegment:any = 'login';

  async databarang() {
    const modal = await this.modalController.create({
      component: DatabarangPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async login() {
    const modal = await this.modalController.create({
      component: LoginPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async laporan() {
    const modal = await this.modalController.create({
      component: LaporanPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async transaksi() {
    const modal = await this.modalController.create({
      component: TransaksiPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  
}
