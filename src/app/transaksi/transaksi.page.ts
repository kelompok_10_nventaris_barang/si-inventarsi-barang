import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DashboardPage } from '../dashboard/dashboard.page';
import { DetailtransaksiPage } from '../detailtransaksi/detailtransaksi.page';
import { TambahtransaksiPage } from '../tambahtransaksi/tambahtransaksi.page';

@Component({
  selector: 'app-transaksi',
  templateUrl: './transaksi.page.html',
  styleUrls: ['./transaksi.page.scss'],
})
export class TransaksiPage implements OnInit {

  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }
  async dashboard() {
    const modal = await this.modalController.create({
      component: DashboardPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async tambahtransaksi() {
    const modal = await this.modalController.create({
      component: TambahtransaksiPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async detailtransaksi() {
    const modal = await this.modalController.create({
      component: DetailtransaksiPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
