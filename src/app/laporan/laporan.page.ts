import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DashboardPage } from '../dashboard/dashboard.page';
import { DetaillaporanPage } from '../detaillaporan/detaillaporan.page';
import { TambahlaporanPage } from '../tambahlaporan/tambahlaporan.page';

@Component({
  selector: 'app-laporan',
  templateUrl: './laporan.page.html',
  styleUrls: ['./laporan.page.scss'],
})
export class LaporanPage implements OnInit {

  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }
  async dashboard() {
    const modal = await this.modalController.create({
      component: DashboardPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async tambahlaporan() {
    const modal = await this.modalController.create({
      component: TambahlaporanPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async detaillaporan() {
    const modal = await this.modalController.create({
      component: DetaillaporanPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
