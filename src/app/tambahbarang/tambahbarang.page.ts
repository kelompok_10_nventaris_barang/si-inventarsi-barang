import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DatabarangPage } from '../databarang/databarang.page';

@Component({
  selector: 'app-tambahbarang',
  templateUrl: './tambahbarang.page.html',
  styleUrls: ['./tambahbarang.page.scss'],
})
export class TambahbarangPage implements OnInit {

  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }
  async databarang() {
    const modal = await this.modalController.create({
      component: DatabarangPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
