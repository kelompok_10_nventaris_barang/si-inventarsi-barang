import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  
  {
    path: 'transaksi',
    loadChildren: () => import('./transaksi/transaksi.module').then( m => m.TransaksiPageModule)
  },
  {
    path: 'laporan',
    loadChildren: () => import('./laporan/laporan.module').then( m => m.LaporanPageModule)
  },
  {
    path: 'databarang',
    loadChildren: () => import('./databarang/databarang.module').then( m => m.DatabarangPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'tambahbarang',
    loadChildren: () => import('./tambahbarang/tambahbarang.module').then( m => m.TambahbarangPageModule)
  },
  {
    path: 'detailbarang',
    loadChildren: () => import('./detailbarang/detailbarang.module').then( m => m.DetailbarangPageModule)
  },
  {
    path: 'tambahtransaksi',
    loadChildren: () => import('./tambahtransaksi/tambahtransaksi.module').then( m => m.TambahtransaksiPageModule)
  },
  {
    path: 'detailtransaksi',
    loadChildren: () => import('./detailtransaksi/detailtransaksi.module').then( m => m.DetailtransaksiPageModule)
  },
  {
    path: 'tambahlaporan',
    loadChildren: () => import('./tambahlaporan/tambahlaporan.module').then( m => m.TambahlaporanPageModule)
  },
  {
    path: 'detaillaporan',
    loadChildren: () => import('./detaillaporan/detaillaporan.module').then( m => m.DetaillaporanPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules,scrollPositionRestoration: 'enabled' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
